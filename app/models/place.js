import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({
  title: validator('presence', true),
  latitude: validator('presence', true),
  longitude: validator('presence', true),
  shortDescription: validator('presence', true),
  description: [
    validator('presence', true),
    validator('length', {
      min: 10
    })
  ],
  email: [
    validator('presence', true),
    validator('format', {
      type: 'email'
    })
  ],
  address: validator('presence', true)
});

export default Model.extend(Validations, {
  title: attr('string'),
  description: attr('string'),
  shortDescription: attr('string'),
  email: attr('string'),
  address: attr('string'),
  website: attr('string'),
  latitude: attr('number'),
  longitude: attr('number'),
  user: belongsTo('user'),
  attachments: hasMany('attachment'),
  isFeatured: attr('boolean'),
  likes: hasMany('like')
});
