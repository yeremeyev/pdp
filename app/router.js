import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('login');
  this.route('signup');
  this.route('places', function() {
    this.route('new');
    this.route('view', { path: ':id' });
    this.route('edit');
  });
  this.route('not-found', { path: '/*path' });
});

export default Router;
