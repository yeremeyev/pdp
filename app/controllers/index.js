import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['lat', 'lng', 'zoom'],

  lat: 55,
  lng: 50,
  zoom: 4,
  featured: Ember.computed.filterBy('places', 'isFeatured'),
  loading: false,

  actions: {
    updatePosition(lat, lng, zoom, bounds) {
      this.setProperties({ lat, lng, zoom, loading: true });
      this.store
        .query('place', { bounds })
        .then(places => this.setProperties({places, loading: false}));
    }
  }
});
