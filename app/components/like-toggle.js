import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'span',

  session: Ember.inject.service(),

  liked: Ember.computed('session', 'likes.[]', function () {
    const likes = this.get('likes');
    const user = this.get('session').get('currentUser');
    return likes.find(like => like.user.id === user.id);
  }),

  actions: {
    toggle() {
      const liked = this.get('liked');
      if (liked) {
        liked.destroyRecord();
      } else {
        const user = this.get('session').get('currentUser');
        this.get('likes').createRecord({ user }).save();
      }
    }
  }
});
