import Ember from 'ember';

export default Ember.Component.extend({
  classNames: 'search-box',
  query: '',

  actions: {
    submit(value) {
      this.attrs.onSubmit(value);
    },

    clear() {
      this.set('query', '');
      this.attrs.onSubmit();
    }
  }
});
