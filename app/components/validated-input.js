import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['field'],
  classNameBindings: ['showErrorClass:error', 'isValid:success'],
  model: null,
  value: null,
  type: 'text',
  valuePath: '',
  placeholder: '',
  validation: null,
  isTyping: false,

  init() {
    this._super(...arguments);

    const valuePath = this.get('valuePath');
    Ember.defineProperty(this, 'validation', Ember.computed.oneWay(`model.validations.attrs.${valuePath}`));
    Ember.defineProperty(this, 'value', Ember.computed.alias(`model.${valuePath}`));
  },

  notValidating: Ember.computed.not('validation.isValidating'),
  didValidate: Ember.computed.oneWay('targetObject.didValidate'),
  hasContent: Ember.computed.notEmpty('value'),
  isValid: Ember.computed.and('hasContent', 'validation.isValid', 'notValidating'),
  isInvalid: Ember.computed.oneWay('validation.isInvalid'),
  showErrorClass: Ember.computed.and('notValidating', 'showMessage', 'hasContent', 'validation'),

  showErrorMessage: Ember.computed('validation.isDirty', 'isInvalid', 'didValidate', function() {
    return (this.get('validation.isDirty') || this.get('didValidate')) && this.get('isInvalid');
  }),

  showWarningMessage: Ember.computed('validation.isDirty', 'validation.warnings.[]', 'isValid', 'didValidate', function() {
    return (this.get('validation.isDirty') || this.get('didValidate')) && this.get('isValid') && !Ember.isEmpty(this.get('validation.warnings'));
  })
});
