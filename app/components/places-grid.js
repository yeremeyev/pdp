import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['ui', 'four', 'stackable', 'doubling', 'cards']
});
