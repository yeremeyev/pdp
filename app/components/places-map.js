import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['ui', 'dimmable'],
  classNameBindings: ['loading:dimmed'],
  loading: false,
  tiles: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',

  markers: Ember.computed('places.[]', 'filter', function() {
    const filter = this.getWithDefault('filter', '');
    const places = this.getWithDefault('places', []);

    return places.map(place => {
      const title = place.get('title');
      const filtered = title && title.toLowerCase().indexOf(filter.toLowerCase()) > -1;
      return {
        place,
        opacity: !filter || filtered ? 1 : 0.5
      };
    });
  }),

  actions: {
    update(e) {
      const map = e.target;
      const center = map.getCenter();
      const bounds = map.getBounds().toBBoxString();
      const zoom = map.getZoom();
      this.attrs.onUpdate(center.lat, center.lng, zoom, bounds);
    },

    applyFilter(query) {
      this.set('filter', query);
    }
  }
});
