import Ember from 'ember';
import EmberUploader from 'ember-uploader';

export default EmberUploader.FileField.extend({
  multiple: true,
  url: '/api/uploads',

  filesDidChange (files) {
    const uploader = EmberUploader.Uploader.create({
      url: this.get('url')
    });

    if (!Ember.isEmpty(files)) {
      uploader.upload(files);
    }

    uploader.on('didUpload', upload => {
      this.attrs.onSuccess(upload);
    });
  }
});
