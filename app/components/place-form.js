import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    updatePosition(e) {
      const { lat, lng } = e.latlng || e.target.getLatLng();
      this.get('place').setProperties({ latitude: lat, longitude: lng });
    },

    addAttachment(attachment) {
      this.get('place').get('attachments').createRecord(attachment);
    },

    submit() {
      const place = this.get('place');
      this.attrs.submit(place);
    }
  }
});
