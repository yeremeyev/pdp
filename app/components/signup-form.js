import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    submit() {
      const user = this.get('user');
      this.attrs.signup(user);
    }
  }
});
