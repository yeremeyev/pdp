import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    submit() {
      const credentials = this.getProperties('identification', 'password');
      this.attrs.login(credentials);
    }
  }
});
