export default function(){
  this.transition(
    this.fromRoute('index'),
    this.toRoute('places'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('index'),
    this.toRoute('login'),
    this.use('toLeft'),
    this.reverse('toRight')
  );

  this.transition(
    this.fromRoute('signup'),
    this.toRoute('login'),
    this.use('toRight'),
    this.reverse('toLeft')
  );
}
