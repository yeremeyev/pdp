import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('place');
  },

  actions: {
    save(place) {
      place.save()
        .then(() => this.transitionTo('places.view', place.id));
    }
  }
});
