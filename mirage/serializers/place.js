import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  serialize(object, request) {
    const json = ApplicationSerializer.prototype.serialize.apply(this, arguments);
    const { page, per_page } = request.queryParams;

    if (!page && !per_page) {
      return json;
    }

    return {
      meta: this.pagination(object.models, request),
      data: this.slicePage(json.data, request)
    };
  },

  pagination(collection, request) {
    const perPage = parseInt(request.queryParams.per_page);
    const total = collection.length;
    const pages = Math.floor(total / perPage + 1);

    return {
      total_pages: pages,
      total_entries: total
    };
  },

  slicePage(collection, request) {
    const perPage = parseInt(request.queryParams.per_page) || 15;
    const page = parseInt(request.queryParams.page) || 1;
    const startIndex = perPage * (page - 1);
    const endIndex = perPage * page;

    return collection.slice(startIndex, endIndex);
  }
});
