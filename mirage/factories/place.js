import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  title() { return faker.company.companyName(); },
  description() { return faker.lorem.paragraph(); },
  shortDescription() { return faker.lorem.sentence(); },
  website() { return faker.internet.domainName(); },
  email() { return faker.internet.email(); },
  address() {
    return `${faker.address.streetAddress()}, ${faker.address.city()}`;
  },
  latitude() { return faker.address.latitude(); },
  longitude() { return faker.address.longitude(); },
  isFeatured() { return faker.random.boolean(); }
});
