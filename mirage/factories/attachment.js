import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  url() {
    const url = faker.image.nightlife();
    const slug = faker.random.number();
    return `${url}?${slug}`; 
  }
});
