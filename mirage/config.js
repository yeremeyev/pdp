export default function() {
  this.namespace = 'api';

  this.get('places', (schema, req) => {
    const bounds = req.queryParams.bounds;

    if (!bounds) {
      return schema.places.all();
    }

    const [
      southwestLng,
      southwestLat,
      northeastLng,
      northeastLat
    ] = bounds.split(',').map(parseFloat);

    return schema.places.where(place => {
      return place.latitude > southwestLat &&
             place.longitude > southwestLng &&
             place.latitude < northeastLat &&
             place.longitude < northeastLng;
    });
  });

  this.get('places/:id');
  this.post('places');

  this.get('attachments/:id');
  this.post('attachments');

  this.get('likes/:id');
  this.post('likes');
  this.delete('likes/:id');

  this.post('session', () => {
    return { token: '5up3r5tr0ng4cc355t0k3n' };
  });

  this.get('users', (schema) => {
    return schema.users.find(1);
  });

  this.post('users');
  this.post('uploads', () => {
    return { url: 'http://nakleuki-shop.umi.ru/images/cms/data/genius.jpg' };
  });
}
