export default function(server) {
  server.createList('place', 100);
  server.create('user');

  server.db.places.forEach(place => {
    server.createList('attachment', 5, {
      placeId: place.id
    });
  });
}
