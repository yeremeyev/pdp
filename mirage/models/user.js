import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  places: hasMany('place'),
  likes: hasMany('like')
});
